//
//  ViewController.swift
//  Algorithm_DataStructure
//
//  Created by Arijit Sarkar on 07/05/20.
//  Copyright © 2020 Arijit Sarkar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var linkedList = LinkedList<String>()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        linkedList.add(item: "Akash")
        linkedList.add(item: "Amit")
        linkedList.add(item: "Sajan")
        linkedList.add(item: "Rajat")
        linkedList.displayAll()
        linkedList.remove(item: "Akash")
        linkedList.displayAll()
        linkedList.remove(item: "Sajan")
        linkedList.displayAll()
    }


}


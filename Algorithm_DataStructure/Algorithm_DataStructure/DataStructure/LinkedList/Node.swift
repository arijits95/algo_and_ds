//
//  Node.swift
//  Algorithm_DataStructure
//
//  Created by Arijit Sarkar on 07/05/20.
//  Copyright © 2020 Arijit Sarkar. All rights reserved.
//

import Foundation

class Node<T: Hashable>: CustomStringConvertible {
    
    var data: T
    var next: Node?
    
    init(data: T) {
        self.data = data
    }
    
    var description: String {
        return "\(data)"
    }
}

//
//  LinkedList.swift
//  Algorithm_DataStructure
//
//  Created by Arijit Sarkar on 07/05/20.
//  Copyright © 2020 Arijit Sarkar. All rights reserved.
//

import Foundation

class LinkedList<T: Hashable> {
    
    var head: Node<T>?
    
    func add(item: T) {
        let node = Node(data: item)
        if let lastNode = getLastNode() {
            lastNode.next = node
        } else {
            head = node
        }
        print("\nITEM \(item) INSERTED")
    }
    
    func displayAll() {
        guard var tempHead = head else {
            print("\nNO ITEMS PRESENT")
            return
        }
        print("\n---------LINKED LIST--------\n")
        while true {
            print(tempHead, separator: "", terminator: "--->")
            if tempHead.next == nil {
                break
            }
            tempHead = tempHead.next!
        }
        print("\n-----------------------------\n")
    }
    
    func remove(item: T) {
        guard var currentItem = head else {
            print("\nNO ITEMS PRESENT")
            return
        }
        var itemFound = true
        var lastItem = currentItem
        while currentItem.data != item {
            lastItem = currentItem
            if currentItem.next != nil {
                currentItem = currentItem.next!
            } else {
                itemFound = false
                break
            }
        }
        if itemFound {
            if currentItem === lastItem {
                head = currentItem.next
            } else {
                lastItem.next = currentItem.next
            }
            currentItem.next = nil
            print("\nITEM \(item) DELETED")
        } else {
            print("\nITEM \(item) NOT FOUND")
        }
    }
    
    private func getLastNode() -> Node<T>? {
        guard head != nil else { return nil }
        var tempHead = head
        while tempHead?.next != nil {
            tempHead = tempHead?.next
        }
        return tempHead
    }
    
}
